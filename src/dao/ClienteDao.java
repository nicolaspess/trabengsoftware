package dao;

import dao.GenericDao;
import models.Cliente;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 13/10/2016.
 */
public class ClienteDao extends GenericDao {

    public void salvar(Cliente cliente) throws SQLException {
        String insert = "INSERT INTO cliente(nome, cpf, fidelidade, pontos) VALUES(?,?,?,?)";
        save(insert, cliente.getNome(), cliente.getCpf(),cliente.getFidelidade(), cliente.getPontos());
    }

    public void alterar(Cliente cliente) throws SQLException {
        String update = "UPDATE cliente " +
                "SET nome = ?, cpf = ?, fidelidade = ?, pontos = ? " +
                "WHERE id = ?";
        update(update, cliente.getNome(), cliente.getCpf(),cliente.getFidelidade(), cliente.getPontos(), cliente.getId());
    }

    public void excluir(long id) throws SQLException {
        String delete = "DELETE FROM cliente WHERE id = ?";
        delete(delete, id);
    }

    public List<Cliente> selecionaCliente(String nome) throws SQLException{
        String select = "SELECT * FROM cliente WHERE nome LIKE ?";
        Cliente cliente = null;
        List<Cliente> clientes = new ArrayList<Cliente>();

        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, "%"+nome+"%");
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            cliente = new Cliente();
            cliente.setId(rs.getInt("id"));
            cliente.setNome(rs.getString("nome"));
            cliente.setCpf(rs.getString("cpf"));
            cliente.setFidelidade(rs.getInt("fidelidade"));
            cliente.setPontos(rs.getInt("pontos"));
            clientes.add(cliente);
        }
        rs.close();
        stmt.close();
        return clientes;
    }

    public Cliente selecionaClienteCpf(String cpf) throws SQLException{
        String select = "SELECT * FROM cliente WHERE cpf = ?";
        Cliente cliente = null;


        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, cpf);
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            cliente = new Cliente();
            cliente.setId(rs.getInt("id"));
            cliente.setNome(rs.getString("nome"));
            cliente.setCpf(rs.getString("cpf"));
            cliente.setFidelidade(rs.getInt("fidelidade"));
            cliente.setPontos(rs.getInt("pontos"));
        }
        rs.close();
        stmt.close();
        return cliente;
    }

    public List<Cliente> selecionaClientes() throws SQLException{
        List<Cliente> clientes = new ArrayList<Cliente>();
        String select = "SELECT * FROM cliente";

        PreparedStatement stmt = getConnection().prepareStatement(select);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()){
            Cliente cliente = new Cliente();
            cliente.setId(rs.getInt("id"));
            cliente.setNome(rs.getString("nome"));
            cliente.setCpf(rs.getString("cpf"));
            cliente.setFidelidade(rs.getInt("fidelidade"));
            cliente.setPontos(rs.getInt("pontos"));
            clientes.add(cliente);
        }
        rs.close();
        stmt.close();

        return clientes;
    }
}
