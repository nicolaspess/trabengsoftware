package dao;

import models.Produto;
import models.Perfumaria;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 28/10/2016.
 */
public class PerfumariaDao extends GenericDao{
    public void salvar(Perfumaria produto) throws SQLException {

        String insert = "INSERT INTO produto(nome, tipo, preco, fornecedor, quantidade, validade, preco_em_pontos) VALUES(?,?,?,?,?,?,?)";
        save(insert, produto.getNome(), "perfumaria", produto.getPreco(), produto.getFornecedor(), produto.getQuantidade(), produto.getValidade(), produto.getPrecoEmPontos());


    }

    public void alterar(Perfumaria produto) throws SQLException {

        String update = "UPDATE produto " +
        "SET nome = ?, preco = ?, fornecedor = ?, quantidade = ?, validade = ?, preco_em_pontos = ? " +
        "WHERE id = ?";
        update(update, produto.getNome(), produto.getPreco(), produto.getFornecedor(), produto.getQuantidade(),produto.getValidade(), produto.getPrecoEmPontos(), produto.getId());
    }

    public void excluir(long id) throws SQLException {
        String delete = "DELETE FROM produto WHERE id = ?";
        delete(delete, id);
    }

    public List<Perfumaria> selecionaPerfumaria(String nome) throws SQLException{
        String select = "SELECT * FROM produto WHERE nome LIKE ? and tipo = 'perfumaria'";
        List<Perfumaria> perfumarias = new ArrayList<Perfumaria>();

        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, "%"+nome+"%");
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            Perfumaria perfumaria = new Perfumaria();
            perfumaria.setId(rs.getInt("id"));
            perfumaria.setNome(rs.getString("nome"));
            perfumaria.setPreco(rs.getDouble("preco"));
            perfumaria.setFornecedor(rs.getString("fornecedor"));
            perfumaria.setQuantidade(rs.getInt("quantidade"));
            perfumaria.setValidade(rs.getString("validade"));
            perfumaria.setPrecoEmPontos(rs.getInt("preco_em_pontos"));
            perfumarias.add(perfumaria);
        }
        rs.close();
        stmt.close();
        return perfumarias;
    }


    public List<Perfumaria> selecionaPerfumarias() throws SQLException{
        List<Perfumaria> perfumarias = new ArrayList<Perfumaria>();
        String select = "SELECT * FROM produto where tipo = 'perfumaria'";

        PreparedStatement stmt = getConnection().prepareStatement(select);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()){

            Perfumaria perf = new Perfumaria();
            perf.setId(rs.getInt("id"));
            perf.setNome(rs.getString("nome"));
            perf.setPreco(rs.getDouble("preco"));
            perf.setQuantidade(rs.getInt("quantidade"));
            perfumarias.add(perf);
        }
        rs.close();
        stmt.close();

        return perfumarias;
    }

}
