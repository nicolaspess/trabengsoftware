package dao;

import models.Funcionario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 22/10/2016.
 */
public class FuncionarioDao extends GenericDao{

    public Funcionario logar(String user, String pass) throws SQLException{
        String select = "SELECT * FROM funcionario WHERE usuario = ?";
        Funcionario func = null;

        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, user);
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            func = new Funcionario();
            func.setId(rs.getInt("id"));
            func.setNome(rs.getString("nome"));
            func.setFuncao(rs.getString("funcao"));
            func.setUsuario(rs.getString("usuario"));
            func.setSenha(rs.getString("senha"));
        }
        rs.close();
        stmt.close();
        return func;
    }

    public void salvar(Funcionario func) throws SQLException {
        String insert = "INSERT INTO funcionario(nome, funcao, usuario, senha) VALUES(?,?,?,?)";
        save(insert, func.getNome(), func.getFuncao(), func.getUsuario(), func.getSenha());
    }

    public void alterar(Funcionario func) throws SQLException {
        String update = "UPDATE funcionario " +
                "SET nome = ?, funcao = ?, usuario = ?, senha = ? " +
                "WHERE id = ?";
        update(update, func.getNome(), func.getFuncao(), func.getUsuario(), func.getSenha(), func.getId());
    }

    public void excluir(long id) throws SQLException {
        String delete = "DELETE FROM funcionario WHERE id = ?";
        delete(delete, id);
    }

    public List<Funcionario> selecionaFuncionario(String nome) throws SQLException{
        String select = "SELECT * FROM funcionario WHERE nome LIKE ?";
        Funcionario func = null;
        List<Funcionario> funcinarios = new ArrayList<Funcionario>();

        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, "%"+nome+"%");
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            func = new Funcionario();
            func.setId(rs.getInt("id"));
            func.setNome(rs.getString("nome"));
            func.setFuncao(rs.getString("funcao"));
            func.setUsuario(rs.getString("usuario"));
            func.setSenha(rs.getString("senha"));
            funcinarios.add(func);
        }
        rs.close();
        stmt.close();
        return funcinarios;
    }

    public List<Funcionario> selecionaFuncionarios() throws SQLException{
        List<Funcionario> funcionarios = new ArrayList<Funcionario>();
        String select = "SELECT * FROM FUNCIONARIO";

        PreparedStatement stmt = getConnection().prepareStatement(select);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()){
            Funcionario func = new Funcionario();
            func.setId(rs.getInt("id"));
            func.setNome(rs.getString("nome"));
            func.setFuncao(rs.getString("funcao"));
            func.setSenha(rs.getString("senha"));
            funcionarios.add(func);
        }
        rs.close();
        stmt.close();

        return funcionarios;
    }
}
