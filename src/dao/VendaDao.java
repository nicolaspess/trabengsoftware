package dao;

import models.Produto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by nicolas on 29/11/2016.
 */
public class VendaDao extends GenericDao {

    public void salvar(Produto produto, int idVenda) throws SQLException{
        String insert = "INSERT INTO venda(id_venda, id_produto, quantidade) values (?,?,?)";
        save(insert, idVenda, produto.getId(), produto.getQuantidade());
        atualizaEstoque(produto.getId(), produto.getQuantidade());
    }
    public void salvar(Produto produto, int idVenda, int idCliente) throws SQLException{
        String insert = "INSERT INTO venda(id_venda, id_cliente, id_produto, quantidade) values (?,?,?,?)";
        save(insert, idVenda, idCliente, produto.getId(), produto.getQuantidade());
        atualizaEstoque(produto.getId(), produto.getQuantidade());
    }

    public void atualizaEstoque(int idProduto, int quantidade) throws SQLException{
        String update = "UPDATE produto SET quantidade = quantidade - ? WHERE id = ?";
        update(update,quantidade,idProduto);
    }
}
