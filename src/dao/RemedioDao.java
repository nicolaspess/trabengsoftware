package dao;

import models.Produto;
import models.Remedio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 28/10/2016.
 */
public class RemedioDao extends GenericDao{
    public void salvar(Remedio produto) throws SQLException {

        String insert = "INSERT INTO produto (nome, tipo, preco, fornecedor, quantidade, validade, principio_ativo, dose, laboratorio, generico) VALUES(?,'remedio',?,?,?,?,?,?,?,?)";
        save(insert, produto.getNome(), produto.getPreco(), produto.getFornecedor(), produto.getQuantidade(), produto.getValidade(), produto.getPrincipioAtivo(), produto.getDose(), produto.getLaboratorio(), produto.getGenerico());
        //}else if (produto instanceof Perfumaria){
        //    String insert = "INSERT INTO PRODUTO(nome, tipo, preco, fornecedor, quantidade, validade, preco_em_pontos) VALUES(?,?,?,?,?,?,?)";
        //    save(insert, produto.getNome(), "perfumaria", produto.getPreco(), produto.getFornecedor(), produto.getQuantidade(), produto.getValidade(), ((Perfumaria) produto).getPrecoEmPontos());
        //}

    }

    public void alterar(Remedio remedio) throws SQLException {

        String update = "UPDATE produto " +
                "SET nome = ?, preco = ?, fornecedor = ?, quantidade = ?, validade = ?, principio_ativo = ?, dose = ?, laboratorio = ?, generico = ? " +
                "WHERE id = ?";
        update(update, remedio.getNome(), remedio.getPreco(), remedio.getFornecedor(), remedio.getQuantidade(), remedio.getValidade(), remedio.getPrincipioAtivo(), remedio.getDose(), remedio.getLaboratorio(), remedio.getGenerico(), remedio.getId());
        //}else if (produto instanceof Perfumaria){
        //    String update = "UPDATE PRODUTO " +
        //            "SET nome = ?, preco = ?, fornecedor = ?, quantidade = ?, validade = ?, preco_em_pontos = ? " +
        //            "WHERE id = ?";
        //    update(update, produto.getId(), produto.getNome(), produto.getPreco(), produto.getFornecedor(), produto.getQuantidade(),produto.getValidade(), ((Perfumaria) produto).getPrecoEmPontos());
        //}

    }

    public void excluir(long id) throws SQLException {
        String delete = "DELETE FROM produto WHERE id = ?";
        delete(delete, id);
    }

    public List<Remedio> selecionaRemedio(String nome) throws SQLException{
        String select = "SELECT * FROM produto WHERE nome LIKE ? and tipo = 'remedio'";
        List<Remedio> remedios = new ArrayList<Remedio>();

        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setString(1, "%"+nome+"%");
        ResultSet rs = stmt.executeQuery();

        while(rs.next()){
            Remedio remedio = new Remedio();
            remedio.setId(rs.getInt("id"));
            remedio.setNome(rs.getString("nome"));
            remedio.setPreco(rs.getDouble("preco"));
            remedio.setFornecedor(rs.getString("fornecedor"));
            remedio.setQuantidade(rs.getInt("quantidade"));
            remedio.setValidade(rs.getString("validade"));
            remedio.setPrincipioAtivo(rs.getString("principio_ativo"));
            remedio.setLaboratorio(rs.getString("laboratorio"));
            remedio.setDose(rs.getString("dose"));
            remedio.setGenerico( (rs.getInt("generico") == 1) );
            remedios.add(remedio);
        }
        rs.close();
        stmt.close();
        return remedios;
    }


    public List<Remedio> selecionaRemedios() throws SQLException{
        List<Remedio> remedios = new ArrayList<Remedio>();
        String select = "SELECT * FROM produto WHERE tipo = 'remedio'";

        PreparedStatement stmt = getConnection().prepareStatement(select);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()){
            Remedio remedio = new Remedio();
            remedio.setId(rs.getInt("id"));
            remedio.setNome(rs.getString("nome"));
            remedio.setPreco(rs.getDouble("preco"));
            remedio.setQuantidade(rs.getInt("quantidade"));
            remedios.add(remedio);
        }
        rs.close();
        stmt.close();

        return remedios;
    }
}
