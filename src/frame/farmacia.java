package frame; /**
 * Created by nicolas on 13/10/2016.
 */

import javax.security.auth.login.LoginContext;
import javax.swing.*;
import java.security.Principal;

public class farmacia {

    private static VendaForm VendaGUI;
    private static LoginForm LoginGUI;
    private static CadastrarMedicamentoForm CadastrarMedicamentoGUI;
    private static CadastrarPerfumariaForm CadastrarPerfumariaGUI;
    private static PesquisaProdutoEmEstoqueForm PesquisaProdutoEmEstoqueGUI;
    private static CadastrarProdutoForm CadastrarProdutoGUI;
    private static PrincipalForm PrincipalGUI;
    private static FarmaciaMenu BarraDeMenu;
    private static JFrame Frame;

    // Dados de login
    private static String usuario;
    private static String senha;
    private static String funcao;
    private static String nome;

    public static void CadastrarPerfumaria()
    {
        CadastrarPerfumariaGUI = new CadastrarPerfumariaForm();
        Frame.setContentPane(CadastrarPerfumariaGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void CadastrarMedicamento()
    {
        CadastrarMedicamentoGUI = new CadastrarMedicamentoForm();
        Frame.setContentPane(CadastrarMedicamentoGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void PesquisaProdutoEmEstoque()
    {
        PesquisaProdutoEmEstoqueGUI = new PesquisaProdutoEmEstoqueForm();
        Frame.setContentPane(PesquisaProdutoEmEstoqueGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void CadastrarProduto()
    {
        CadastrarProdutoGUI = new CadastrarProdutoForm();
        Frame.setContentPane(CadastrarProdutoGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void SetMenuBar() {
        BarraDeMenu = new FarmaciaMenu();
        Frame.setJMenuBar(BarraDeMenu.getMenubar());
    }

    public static void Login() {
        LoginGUI = new LoginForm();
        Frame.setContentPane(LoginGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void Principal() {
        PrincipalGUI = new PrincipalForm();
        Frame.setContentPane(PrincipalGUI.getPane());
        farmacia.SetMenuBar();
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void VenderProduto() {
        VendaGUI = new VendaForm();
        Frame.setContentPane(VendaGUI.getPane());
        Frame.pack();
        Frame.setVisible(true);
    }

    public static void main(String[] args){
        Frame = new JFrame("FarmaSoft");
        Frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        farmacia.Login();
    }

    public static String getFuncao() {
        return funcao;
    }

    public static void setFuncao(String funcao) {
        farmacia.funcao = funcao;
    }

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String senha) {
        farmacia.senha = senha;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        farmacia.usuario = usuario;
    }

    public static String getNome() {
        return nome;
    }

    public static void setNome(String nome) {
        farmacia.nome = nome;
    }
}
