package frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

import controllers.ControleRemedio;

/**
 * Created by digos on 28/10/16.
 */
public class CadastrarMedicamentoForm {
    private JPanel CadastrarMedicamentoFormView;
    private JTextField inputNomeComercial;
    private JTextField inputPrincipioAtivo;
    private JTextField inputDose;
    private JTextField inputLaboratorio;
    private JTextField inputPrecoUnitario;
    private JTextField inputFornecedor;
    private JTextField inputQuantidadeEmEstoque;
    private JTextField inputValidade;
    private JLabel lblNomeComercial;
    private JLabel lblPrincipioAtivo;
    private JLabel lblDose;
    private JLabel lblLaboratorio;
    private JLabel lblPrecoUnitario;
    private JLabel lblFornecedor;
    private JLabel lblQuantidadeEmEstoque;
    private JLabel lblValidade;
    private JLabel lblErroNomeComercial;
    private JLabel lblErroPrincipioAtivo;
    private JLabel lblErroDose;
    private JLabel lblErroLaboratorio;
    private JLabel lblErroPrecoUnitario;
    private JLabel lblErroFornecedor;
    private JLabel lblErroQuantidadeEmEstoque;
    private JLabel lblErroValidade;
    private JCheckBox chkVendaComRetencaoDeReceita;
    private JButton btnCadastrarMedicamento;
    private JCheckBox chkGenerico;
    private JPanel FormPanel;
    private JPanel SucessPanel;

    public JPanel getPane() {
        return CadastrarMedicamentoFormView;
    }
    public CadastrarMedicamentoForm() {
        btnCadastrarMedicamento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                boolean isError = false;
                String dataDeVencimento = "";
                DateFormat formatoValidadeInput = new SimpleDateFormat("dd/MM/yyyy");
                Format formatoValidadeBD = new SimpleDateFormat("yyyy-MM-dd");

                FormPanel.setVisible(true);
                SucessPanel.setVisible(false);
                lblErroNomeComercial.setVisible(false);
                lblErroPrincipioAtivo.setVisible(false);
                lblErroDose.setVisible(false);
                lblErroLaboratorio.setVisible(false);
                lblErroPrecoUnitario.setVisible(false);
                lblErroFornecedor.setVisible(false);
                lblErroQuantidadeEmEstoque.setVisible(false);
                lblErroValidade.setVisible(false);


                // Verificando se capos estão vazios
                if (inputNomeComercial.getText().isEmpty())
                {
                    lblErroNomeComercial.setText("Nome Comercial deve ser preenchido!");
                    lblErroNomeComercial.setVisible(true);
                    isError = true;
                }
                if (inputPrincipioAtivo.getText().isEmpty())
                {
                    lblErroPrincipioAtivo.setText("Princípio Ativo deve ser preenchido!");
                    lblErroPrincipioAtivo.setVisible(true);
                    isError = true;
                }
                if (inputDose.getText().isEmpty())
                {
                    lblErroDose.setText("Dose deve ser preenchida!");
                    lblErroDose.setVisible(true);
                    isError = true;
                }
                if (inputLaboratorio.getText().isEmpty())
                {
                    lblErroLaboratorio.setText("Laboratório deve ser preenchido!");
                    lblErroLaboratorio.setVisible(true);
                    isError = true;
                }
                if (inputPrecoUnitario.getText().isEmpty())
                {
                    lblErroPrecoUnitario.setText("Preço unitario deve ser preenchido!");
                    lblErroPrecoUnitario.setVisible(true);
                    isError = true;
                }
                if (inputFornecedor.getText().isEmpty())
                {
                    lblErroFornecedor.setText("Fornecedor deve ser preenchido!");
                    lblErroFornecedor.setVisible(true);
                    isError = true;
                }
                if (inputQuantidadeEmEstoque.getText().isEmpty())
                {
                    lblErroQuantidadeEmEstoque.setText("Quantidade em Estoque deve ser preenchida!");
                    lblErroQuantidadeEmEstoque.setVisible(true);
                    isError = true;
                }
                if (inputValidade.getText().isEmpty())
                {
                    lblErroValidade.setText("Validade deve ser preenchido!");
                    lblErroValidade.setVisible(true);
                    isError = true;
                }


                // Verificando se é possivel fazer conversão dos tipos de dados
                if (!isError)
                {

                    try {
                        Double.parseDouble(inputPrecoUnitario.getText());
                    }
                    catch (NumberFormatException ne) {
                        lblErroPrecoUnitario.setText("Preço unitario deve ser no formato: 100.00!");
                        lblErroPrecoUnitario.setVisible(true);
                        isError = true;
                    }

                    try {
                        Integer.parseInt(inputQuantidadeEmEstoque.getText());
                    }
                    catch (NumberFormatException ne)
                    {
                        lblErroQuantidadeEmEstoque.setText("Quantidade com valor inválido!");
                        lblErroQuantidadeEmEstoque.setVisible(true);
                        isError = true;
                    }
                    
                    try {
                        dataDeVencimento = formatoValidadeBD.format(formatoValidadeInput.parse(inputValidade.getText()));
                    } catch (ParseException pe) {
                        lblErroValidade.setText("Validade deve estar no formato dd/mm/aaaa!");
                        lblErroValidade.setVisible(true);
                        isError = true;
                    }


                    // Por fim validamos se os valores estão nos intervalos corretos
                    if (!isError) {

                        if (Double.parseDouble(inputPrecoUnitario.getText()) <= 0) {
                            lblErroPrecoUnitario.setText("Preço unitario com valor inválido!");
                            lblErroPrecoUnitario.setVisible(true);
                            isError = true;
                        }

                        if (Integer.parseInt(inputQuantidadeEmEstoque.getText()) <= 0) {
                            lblErroQuantidadeEmEstoque.setText("Quantidade com valor inválido!");
                            lblErroQuantidadeEmEstoque.setVisible(true);
                            isError = true;
                        }


                        // Se nenhum erro até aqui, inserimos no BD
                        if (!isError) {

                            ControleRemedio controller = new ControleRemedio();

                            // TODO: adicionar se medicamento necessita de receita: chkVendaComRetencaoDeReceita.isSelected()

                            try {
                                controller.salvar(inputNomeComercial.getText(),
                                        Double.parseDouble(inputPrecoUnitario.getText()),
                                        inputFornecedor.getText(),
                                        Integer.parseInt(inputQuantidadeEmEstoque.getText()),
                                        dataDeVencimento,
                                        inputPrincipioAtivo.getText(),
                                        inputDose.getText(),
                                        inputLaboratorio.getText(),
                                        chkGenerico.isSelected());
                            }
                            catch (SQLException SQLe) {
                                System.out.println("Erro ao salvar dados no BD");
                                isError = true;
                            }

                            if (!isError) {
                                FormPanel.setVisible(false);
                                SucessPanel.setVisible(true);
                            }
                        }
                    }
                }
            }
        });
    }
}
