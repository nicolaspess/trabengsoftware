package frame;

import controllers.ControlePerfumaria;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by digos on 28/10/16.
 */
public class CadastrarPerfumariaForm {
    private JPanel CadastrarPerfumariaFormView;
    private JTextField inputNomeComercial;
    private JTextField inputPrecoEmDinheiro;
    private JTextField inputPrecoEmPontosDeFidelidade;
    private JTextField inputFornecedor;
    private JTextField inputQuantidadeEmEstoque;
    private JTextField inputValidade;
    private JButton btnCadastrarItemDePerfumaria;
    private JLabel lblNomeComercial;
    private JLabel lblPrecoEmDinheiro;
    private JLabel lbPrecoEmPontosDeFidelidade;
    private JLabel lblFornecedor;
    private JLabel lblQuantidadeEmEstoque;
    private JLabel lblValidade;
    private JLabel lblErroValidade;
    private JLabel lblErroQuantidadeEmEstoque;
    private JLabel lblErroFornecedor;
    private JLabel lblErroPrecoEmPontosDeFidelidade;
    private JLabel lblErroPrecoEmDinheiro;
    private JLabel lblErroNomeComercial;
    private JPanel SucessPanel;
    private JPanel FormPanel;

    public JPanel getPane() {
        return CadastrarPerfumariaFormView;
    }

    public CadastrarPerfumariaForm() {
        btnCadastrarItemDePerfumaria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean isError = false;
                String dataDeVencimento = "";
                DateFormat formatoValidadeInput = new SimpleDateFormat("dd/MM/yyyy");
                Format formatoValidadeBD = new SimpleDateFormat("yyyy-MM-dd");

                FormPanel.setVisible(true);
                SucessPanel.setVisible(false);
                lblErroNomeComercial.setVisible(false);
                lblErroPrecoEmDinheiro.setVisible(false);
                lblErroPrecoEmPontosDeFidelidade.setVisible(false);
                lblErroFornecedor.setVisible(false);
                lblErroQuantidadeEmEstoque.setVisible(false);
                lblErroValidade.setVisible(false);


                // Verificando se capos estão vazios
                if (inputNomeComercial.getText().isEmpty())
                {
                    lblErroNomeComercial.setText("Nome Comercial deve ser preenchido!");
                    lblErroNomeComercial.setVisible(true);
                    isError = true;
                }
                if (inputPrecoEmDinheiro.getText().isEmpty())
                {
                    lblErroPrecoEmDinheiro.setText("Preço deve ser preenchido!");
                    lblErroPrecoEmDinheiro.setVisible(true);
                    isError = true;
                }
                if (inputPrecoEmPontosDeFidelidade.getText().isEmpty())
                {
                    lblErroPrecoEmPontosDeFidelidade.setText("Preço em pontos deve ser preenchido!");
                    lblErroPrecoEmPontosDeFidelidade.setVisible(true);
                    isError = true;
                }
                if (inputFornecedor.getText().isEmpty())
                {
                    lblErroFornecedor.setText("Fornecedor deve ser preenchido!");
                    lblErroFornecedor.setVisible(true);
                    isError = true;
                }
                if (inputQuantidadeEmEstoque.getText().isEmpty())
                {
                    lblErroQuantidadeEmEstoque.setText("Quantidade em Estoque deve ser preenchido!");
                    lblErroQuantidadeEmEstoque.setVisible(true);
                    isError = true;
                }
                if (inputValidade.getText().isEmpty())
                {
                    lblErroValidade.setText("Validade deve ser preenchido!");
                    lblErroValidade.setVisible(true);
                    isError = true;
                }


                // Verificando se é possivel fazer conversão dos tipos de dados
                if (!isError)
                {

                    try {
                        Double.parseDouble(inputPrecoEmDinheiro.getText());
                    }
                    catch (NumberFormatException ne) {
                        lblErroPrecoEmDinheiro.setText("Preço deve ser no formato: 100.00!");
                        lblErroPrecoEmDinheiro.setVisible(true);
                        isError = true;
                    }

                    try {
                        Integer.parseInt(inputPrecoEmPontosDeFidelidade.getText());
                    }
                    catch (NumberFormatException ne) {
                        lblErroPrecoEmPontosDeFidelidade.setText("Preço em pontos inválido");
                        lblErroPrecoEmPontosDeFidelidade.setVisible(true);
                        isError = true;
                    }

                    try {
                        Integer.parseInt(inputQuantidadeEmEstoque.getText());
                    }
                    catch (NumberFormatException ne)
                    {
                        lblErroQuantidadeEmEstoque.setText("Quantidade com valor inválido!");
                        lblErroQuantidadeEmEstoque.setVisible(true);
                        isError = true;
                    }

                    try {
                        dataDeVencimento = formatoValidadeBD.format(formatoValidadeInput.parse(inputValidade.getText()));
                    } catch (ParseException pe) {
                        lblErroValidade.setText("Validade deve estar no formato dd/mm/aaaa!");
                        lblErroValidade.setVisible(true);
                        isError = true;
                    }


                    // Por fim validamos se os valores estão nos intervalos corretos
                    if (!isError) {

                        if (Double.parseDouble(inputPrecoEmDinheiro.getText()) <= 0) {
                            lblErroPrecoEmDinheiro.setText("Preço com valor inválido!");
                            lblErroPrecoEmDinheiro.setVisible(true);
                            isError = true;
                        }

                        if (Integer.parseInt(inputPrecoEmPontosDeFidelidade.getText()) <= 0) {
                            lblErroPrecoEmPontosDeFidelidade.setText("Preço em pontos com valor inválido!");
                            lblErroPrecoEmPontosDeFidelidade.setVisible(true);
                            isError = true;
                        }

                        if (Integer.parseInt(inputQuantidadeEmEstoque.getText()) <= 0) {
                            lblErroQuantidadeEmEstoque.setText("Quantidade com valor inválido!");
                            lblErroQuantidadeEmEstoque.setVisible(true);
                            isError = true;
                        }


                        // Se nenhum erro até aqui, inserimos no BD
                        if (!isError) {

                            ControlePerfumaria controller = new ControlePerfumaria();

                            try {
                                controller.salvar(inputNomeComercial.getText(),
                                        Double.parseDouble(inputPrecoEmDinheiro.getText()),
                                        inputFornecedor.getText(),
                                        Integer.parseInt(inputQuantidadeEmEstoque.getText()),
                                        dataDeVencimento,
                                        Integer.parseInt(inputPrecoEmPontosDeFidelidade.getText())
                                );
                            }
                            catch (SQLException SQLe) {
                                System.out.println("Erro ao salvar dados no BD");
                                isError = true;
                            }

                            if (!isError)
                            {
                                FormPanel.setVisible(false);
                                SucessPanel.setVisible(true);
                            }
                        }
                    }
                }
            }
        });
    }
}
