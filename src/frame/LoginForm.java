package frame;

import controllers.ControleFuncionario;
import models.Funcionario;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * Created by digos on 28/11/16.
 */
public class LoginForm {
    private JPanel LoginView;
    private JTextField inputUsuario;
    private JPasswordField inputSenha;
    private JButton btnFazerLogin;
    private JLabel lblErro;

    public LoginForm() {

        lblErro.setVisible(false);

        btnFazerLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ControleFuncionario cf = new ControleFuncionario();
                Funcionario func = new Funcionario();

                String passText = inputSenha.getPassword().toString();
                String usuario = inputUsuario.getText();

                try {
                    func = cf.fazerLogin(usuario,passText);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                if(func != null){
                    farmacia.setUsuario(usuario);
                    farmacia.setSenha(passText);
                    farmacia.setFuncao(func.getFuncao());
                    farmacia.setNome(func.getNome());
                    farmacia.Principal();
                }
                else {
                    lblErro.setVisible(true);
                }
            }
        });
    }

    public JPanel getPane() {
        return LoginView;
    }
}
