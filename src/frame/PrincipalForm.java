package frame;

import javax.swing.*;

/**
 * Created by digos on 01/11/16.
 */
public class PrincipalForm {
    private JPanel PrincipalView;
    private JLabel lblBoasVindas;

    public PrincipalForm()
    {
        lblBoasVindas.setText(""+ farmacia.getNome()+", bem-vindo(a) ao FarmaSoft!");
    }

    public JPanel getPane() {
        return PrincipalView;
    }
}

