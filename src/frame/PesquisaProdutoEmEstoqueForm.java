package frame;

import controllers.ControlePerfumaria;
import controllers.ControleRemedio;
import models.Perfumaria;
import models.Remedio;
import sun.misc.Perf;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by digos on 28/10/16.
 */
public class PesquisaProdutoEmEstoqueForm {
    private JPanel PesquisaProdutoEmEstoqueView;
    private JButton btnPesquisarProduto;
    private JTextField inputNomeComercial;
    private JComboBox cbxTipoDeProduto;
    private JLabel lblNomeComercial;
    private JLabel lblTipoDeProduto;
    private JLabel lblErroNomeComercial;
    private JPanel BuscaPanel;
    private JPanel ResultadoMedicamentoPanel;
    private JPanel ResultadoPerfumariaPanel;

    private JTable medicamentoTable;
    private JTable perfumariaTable;
    private JLabel lblPerfumariaResultados;
    private JLabel lblMedicamentoResultados;

    public JPanel getPane(){
        return PesquisaProdutoEmEstoqueView;
    }
    public PesquisaProdutoEmEstoqueForm() {

        btnPesquisarProduto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                boolean isError = false;

                ResultadoMedicamentoPanel.setVisible(false);
                ResultadoPerfumariaPanel.setVisible(false);

                if (cbxTipoDeProduto.getSelectedItem().toString().equals("Medicamento"))
                {
                    List<Remedio> remedios = new ArrayList<Remedio>();
                    ControleRemedio controller = new ControleRemedio();

                    try {
                        remedios = controller.buscaProdutoPorNome(inputNomeComercial.getText());
                    }
                    catch (SQLException SQLe) {
                        System.out.println("Erro na consulta ao banco de dados");
                        SQLe.printStackTrace();
                        isError = true;
                    }

                    if (!isError) {
                        BuscaPanel.setVisible(false);

                        preencheTabelaMedicamentos(remedios);
                        lblMedicamentoResultados.setText(remedios.size() +" resultado(s) para o nome \""+inputNomeComercial.getText()+"\"");

                        ResultadoMedicamentoPanel.setVisible(true);
                        ResultadoPerfumariaPanel.setVisible(false);
                    }
                }
                else if (cbxTipoDeProduto.getSelectedItem().toString().equals("Perfumaria"))
                {
                    List<Perfumaria> perfumes = new ArrayList<Perfumaria>();
                    ControlePerfumaria controller = new ControlePerfumaria();

                    try {
                        perfumes = controller.buscaProdutoPorNome(inputNomeComercial.getText());
                    }
                    catch (SQLException SQLe) {
                        System.out.println("Erro na consulta ao banco de dados");
                        SQLe.printStackTrace();
                        isError = true;
                    }

                    if (!isError) {
                        BuscaPanel.setVisible(false);

                        preencheTabelaPerfumaria(perfumes);
                        lblPerfumariaResultados.setText(perfumes.size() +" resultado(s) para o nome \""+inputNomeComercial.getText()+"\"");

                        ResultadoMedicamentoPanel.setVisible(false);
                        ResultadoPerfumariaPanel.setVisible(true);
                    }
                }
            }
        });



     }


    public void preencheTabelaPerfumaria(List<Perfumaria> perfumes)
    {
        DefaultTableModel model = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model.addColumn("Nome");
        model.addColumn("Preço");
        model.addColumn("Preço em pontos");
        model.addColumn("Fornecedor");
        model.addColumn("Quantidade");
        model.addColumn("Validade");

        for (Perfumaria p : perfumes) {

            model.addRow(
                    new Object[]{
                            p.getNome(),
                            p.getPreco() + "",
                            p.getPrecoEmPontos() + "",
                            p.getFornecedor(),
                            p.getQuantidade() + "",
                            p.getValidade(),
                    }
            );
        }

        perfumariaTable.setModel(model);
        perfumariaTable.repaint();
    }


    public void preencheTabelaMedicamentos(List<Remedio> remedios)
    {
        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model.addColumn("Nome");
        model.addColumn("Preço");
        model.addColumn("Fornecedor");
        model.addColumn("Quantidade");
        model.addColumn("Validade");
        model.addColumn("Principio Ativo");
        model.addColumn("Dose");
        model.addColumn("Laboratório");
        model.addColumn("Genérico");

        for (Remedio r : remedios) {

            model.addRow(
                   new Object[]{
                            r.getNome(),
                            r.getPreco() + "",
                            r.getFornecedor(),
                            r.getQuantidade() + "",
                            r.getValidade(),
                            r.getPrincipioAtivo(),
                            r.getDose(),
                            r.getLaboratorio(),
                            (r.getGenerico() ? "Sim" : "Não")
                    }
            );
        }

        medicamentoTable.setModel(model);
        medicamentoTable.repaint();
    }

}
