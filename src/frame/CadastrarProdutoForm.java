package frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by digos on 01/11/16.
 */
public class CadastrarProdutoForm {
    private JPanel CadastrarProdutoView;
    private JButton btnCadastroMedicamento;
    private JButton btnCadastroPerfumaria;

    public JPanel getPane()
    {
        return CadastrarProdutoView;
    }

    public CadastrarProdutoForm() {
        btnCadastroMedicamento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                farmacia.CadastrarMedicamento();
            }
        });
        btnCadastroPerfumaria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                farmacia.CadastrarPerfumaria();
            }
        });
    }
}
