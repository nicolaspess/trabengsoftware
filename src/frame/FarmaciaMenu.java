package frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by digos on 30/10/16.
 */
public class FarmaciaMenu implements ActionListener, ItemListener {

    private JMenuBar menubar;

    private JMenu vendas;
    private JMenuItem vendaDeProduto;
    private JMenuItem historicoDeVendas;

    private JMenu clientes;
    private JMenuItem cadastrarCliente;

    private JMenu estoque;
    private JMenuItem consultarEstoque;
    private JMenuItem cadastrarProduto;
    private JMenuItem removerProduto;
    private JMenuItem gerarRelatorio;

    private JMenu funcionarios;
    private JMenuItem cadastrarFuncionario;

    public FarmaciaMenu()
    {
        menubar = new JMenuBar();


        if (farmacia.getFuncao().equals("balconista"))
        {
            vendas = new JMenu("Vendas");
            clientes = new JMenu("Clientes");
            estoque = new JMenu("Estoque");

            vendaDeProduto = new JMenuItem("Venda de produto");
            vendaDeProduto.addActionListener(this);

            historicoDeVendas = new JMenuItem("Histórico de vendas");
            historicoDeVendas.addActionListener(this);

            cadastrarCliente = new JMenuItem("Cadastrar cliente");
            cadastrarCliente.addActionListener(this);

            consultarEstoque = new JMenuItem("Consultar estoque");
            consultarEstoque.addActionListener(this);

            vendas.add(vendaDeProduto);
            vendas.add(historicoDeVendas);
            clientes.add(cadastrarCliente);
            estoque.add(consultarEstoque);

            menubar.add(vendas);
            menubar.add(clientes);
            menubar.add(estoque);
        }

        if (farmacia.getFuncao().equals("farmaceutico"))
        {
            estoque = new JMenu("Estoque");

            consultarEstoque = new JMenuItem("Consultar estoque");
            consultarEstoque.addActionListener(this);

            cadastrarProduto = new JMenuItem("Cadastrar produto");
            cadastrarProduto.addActionListener(this);

            removerProduto = new JMenuItem("Remover produto");
            removerProduto.addActionListener(this);

            gerarRelatorio = new JMenuItem("Gerar relatório");
            gerarRelatorio.addActionListener(this);

            estoque.add(consultarEstoque);
            estoque.add(cadastrarProduto);
            estoque.add(removerProduto);
            estoque.add(gerarRelatorio);

            menubar.add(estoque);
        }

        if (farmacia.getFuncao().equals("admin"))
        {
            funcionarios = new JMenu("Funcionarios");
            cadastrarFuncionario = new JMenuItem("Cadastrar funcionário");
            cadastrarFuncionario.addActionListener(this);
            funcionarios.add(cadastrarFuncionario);
            menubar.add(funcionarios);
        }

        if (farmacia.getFuncao().equals("god"))
        {
            vendas = new JMenu("Vendas");
            clientes = new JMenu("Clientes");
            estoque = new JMenu("Estoque");
            funcionarios = new JMenu("Funcionarios");

            vendaDeProduto = new JMenuItem("Venda de produto");
            vendaDeProduto.addActionListener(this);

            historicoDeVendas = new JMenuItem("Histórico de vendas");
            historicoDeVendas.addActionListener(this);

            cadastrarCliente = new JMenuItem("Cadastrar cliente");
            cadastrarCliente.addActionListener(this);

            consultarEstoque = new JMenuItem("Consultar estoque");
            consultarEstoque.addActionListener(this);

            cadastrarProduto = new JMenuItem("Cadastrar produto");
            cadastrarProduto.addActionListener(this);

            removerProduto = new JMenuItem("Remover produto");
            removerProduto.addActionListener(this);

            gerarRelatorio = new JMenuItem("Gerar relatório");
            gerarRelatorio.addActionListener(this);

            cadastrarFuncionario = new JMenuItem("Cadastrar funcionário");
            cadastrarFuncionario.addActionListener(this);

            vendas.add(vendaDeProduto);
            vendas.add(historicoDeVendas);

            clientes.add(cadastrarCliente);

            estoque.add(consultarEstoque);
            estoque.add(cadastrarProduto);
            estoque.add(removerProduto);
            estoque.add(gerarRelatorio);

            funcionarios.add(cadastrarFuncionario);

            menubar.add(estoque);
            menubar.add(funcionarios);
            menubar.add(vendas);
            menubar.add(clientes);
            menubar.add(estoque);
        }

    }

    public JMenuBar getMenubar() {
        return menubar;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(consultarEstoque))
        {
            farmacia.PesquisaProdutoEmEstoque();
        }

        if (e.getSource().equals(cadastrarProduto))
        {
            farmacia.CadastrarProduto();
        }

        if (e.getSource().equals(vendaDeProduto))
        {
            farmacia.VenderProduto();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
