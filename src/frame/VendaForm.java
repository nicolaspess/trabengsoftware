package frame;

import models.*;
import controllers.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by digos on 28/11/16.
 */
public class VendaForm {
    private JPanel VendaView;
    private JPanel IdentificaClientePanel;
    private JPanel BuscaClientePanel;
    private JPanel TipoVendaPanel;
    private JPanel VendaAdicionarProdutoPanel;
    private JPanel VendaCarrinhoPanel;
    private JPanel ResgateCarrinhoPanel;
    private JPanel ResgateAdicionarProdutoPanel;
    private JButton btnClienteCadastrado;
    private JButton btnCadastrarCliente;
    private JButton btnVendaSemCadastro;
    private JTextField inputCPF;
    private JButton btnPesquisarCliente;
    private JButton btnVendaDeProduto;
    private JButton btnResgateDePontos;
    private JTextField inputVendaNomeProdutoSearch;
    private JButton btnVendaPesquisarProdutoSearch;
    private JTable vendaPesquisaProdutoTable;
    private JButton btnVendaAdicionarAoCarrinho;
    private JButton btnVendaCancelar;
    private JTable vendaTabelaCarrinho;
    private JButton btnVendaAdicionarProduto;
    private JButton btnVendaFinalizar;
    private JTable resgateCarrinhoTable;
    private JButton btnResgateAdicionarProduto;
    private JButton lblResgateFinalizarVenda;
    private JTextField inputResgateNomeProduto;
    private JButton btnResgatePesquisarProduto;
    private JTable resgatePesquisaTable;
    private JButton btnResgateAdicionarAoCarrinho;
    private JButton btnResgateCancelar;
    private JLabel lblErroClienteNaoEncontrado;
    private JPanel VendaPesquisaResultado;
    private JLabel lblNomeCliente;
    private JLabel lblTotalVenda;
    private JLabel lblResgateNomeCliente;
    private JLabel lblResgateSaldoPontos;
    private JLabel lblResgateTotalPontos;
    private JPanel ResgateResultadosView;
    private JLabel lblResgateResultadosMensagem;
    private JLabel lblVendaResutadosBusca;
    private JTextField inputVendaQuantidade;
    private JTextField inputResgateQuantidade;
    private JLabel lblVendaErroQuantidade;
    private JLabel lblResgateErroQuantidade;
    private JTextField inputValorPago;
    private JLabel lblErroValorPago;
    private JPanel panelFail;
    private JLabel lblSucesso;
    private JLabel lblTroco;
    private JPanel panelFinish;
    private JPanel panelTroco;
    private JPanel panelSucesso;
    private JLabel lblErroPontosInsuficientes;

    private Venda venda = new Venda();
    private Cliente cliente = new Cliente();

    public JPanel getPane() {
        return VendaView;
    }


    private void HideAll() {
        // Painel onde é perguntado o tipo de cliente
        IdentificaClientePanel.setVisible(false);
        // Painel para buscar cliente pelo cpf
        BuscaClientePanel.setVisible(false);
        // Painel que pergunta se eh venda ou resgate
        TipoVendaPanel.setVisible(false);
        // Painel que permite pesquisar produtos para adicionar à venda
        VendaAdicionarProdutoPanel.setVisible(false);
        // Painel contendo o carrinho de itens à venda
        VendaCarrinhoPanel.setVisible(false);
        // Painel contendo o carrinho de itens para resgate
        ResgateCarrinhoPanel.setVisible(false);
        // Painel que permite pesquisar produtos de perfumaria para adicionar ao carrinho
        ResgateAdicionarProdutoPanel.setVisible(false);
        // Painel da ultima tela
        panelFinish.setVisible(false);
    }

    public VendaForm() {

        // Na inicialização definimos os paineis que serão visiveis/invisiveis
        HideAll();
        // Painel onde é perguntado o tipo de cliente
        IdentificaClientePanel.setVisible(true);
        iniciaTabelaCarrinho();

        // Cliente já cadastrado, fazer pesquisa
        btnClienteCadastrado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                lblErroClienteNaoEncontrado.setVisible(false);
                BuscaClientePanel.setVisible(true);
            }
        });


        // Cliente não cadastrado, chamar método para cadastrar novo cliente
        btnCadastrarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // ainda não implementado
            }
        });


        // Realizar venda sem cadastro, venda por dinheiro
        btnVendaSemCadastro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                // Atualizando alguns campos
                lblNomeCliente.setText("Cliente não cadastrado"); // Nome do cliente
                lblTotalVenda.setText("R$ 0,00");
                // Exibindo painel
                VendaCarrinhoPanel.setVisible(true);
                cliente.setId(0);
            }
        });


        // Opção de pesquisar cliente pelo CPF
        btnPesquisarCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // ainda não implementado
                ControleCliente cc = new ControleCliente();
                Cliente cli = new Cliente();
                cli = null;
                try {
                    cli = cc.buscaClientePorCpf(inputCPF.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                if(cli != null){
                    //cliente encontrado
                    cliente = cli;
                    HideAll();
                    // Exibindo painel
                    TipoVendaPanel.setVisible(true);
                }else{
                    //mensagem de cliente não encontrado
                    lblErroClienteNaoEncontrado.setVisible(true);
                }
            }
        });


        // Aqui é perguntado se a transação é venda de produto ou resgate de pontos
        // Em caso de venda de produto, vamos para o painel apropriado
        btnVendaDeProduto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                if (cliente.getId() == 0) {
                    lblNomeCliente.setText("Cliente não cadastrado");
                }
                else
                {
                    lblNomeCliente.setText(cliente.getNome()); // Nome do cliente
                }
                lblTotalVenda.setText("R$ 0,00");
                VendaCarrinhoPanel.setVisible(true);
            }
        });
        // Em caso de resgate de pontos, vamos para o painel apropriado
        btnResgateDePontos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                if (cliente.getId() == 0)
                {
                    lblResgateNomeCliente.setText("Cliente não cadastrado");
                    lblResgateSaldoPontos.setText(String.valueOf(0));
                }
                else {
                    lblResgateNomeCliente.setText(cliente.getNome());
                    lblResgateSaldoPontos.setText(String.valueOf(cliente.getPontos()));
                }
                lblResgateTotalPontos.setText("0 Pontos");
                ResgateCarrinhoPanel.setVisible(true);

            }
        });


        btnVendaPesquisarProdutoSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                boolean isError = false;
                List<Produto> prods = new ArrayList<Produto>();
                List<Remedio> rems = new ArrayList<Remedio>();
                List<Perfumaria> perfs = new ArrayList<Perfumaria>();
                ControleRemedio cr = new ControleRemedio();
                ControlePerfumaria cp = new ControlePerfumaria();
                try {
                    rems = cr.buscaProdutoPorNome(inputVendaNomeProdutoSearch.getText());
                    prods.addAll(rems);
                }
                catch (SQLException SQLe) {
                    System.out.println("Erro na consulta ao banco de dados");
                    SQLe.printStackTrace();
                    isError = true;
                }
                try {
                    perfs = cp.buscaProdutoPorNome(inputVendaNomeProdutoSearch.getText());
                    prods.addAll(perfs);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                if (!isError) {
                    VendaPesquisaResultado.setVisible(true);
                    preencheTabelaBuscaVendaRemedio(prods);
                    lblVendaResutadosBusca.setText("Resultado(s) para o nome \"" + inputVendaNomeProdutoSearch.getText() + "\"");
                }
            }

        });

        //Adiciona o produto ao carrinho de compras
        btnVendaAdicionarAoCarrinho.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i;
                double total;
                int[] selecao;
                int quantidade = 0;
                int prod_quantidade = 0;

                DefaultTableModel model = (DefaultTableModel) vendaTabelaCarrinho.getModel();

                selecao = vendaPesquisaProdutoTable.getSelectedRows();


                try {
                    quantidade = Integer.parseInt(inputVendaQuantidade.getText());
                }
                catch (Exception e1)
                {
                    lblVendaErroQuantidade.setVisible(true);
                    return;
                }

                if (quantidade <= 0) {
                    lblVendaErroQuantidade.setVisible(true);
                    return;
                }

                lblVendaErroQuantidade.setVisible(false);

                // Verificando se algum produto selecionado possui quantidade invalida com a solicitada
                for(i = 0; i < selecao.length; i++) {
                    prod_quantidade = Integer.parseInt(vendaPesquisaProdutoTable.getValueAt(selecao[i], 4).toString());

                    if (prod_quantidade < quantidade) {
                        lblVendaErroQuantidade.setVisible(true);
                        return;
                    }
                }

                for(i = 0; i < selecao.length; i++)
                {
                    // TODO: se o produto já está adicionado (com o mesmo código) atualiza as quantides somente

                    model.addRow(new Object[]{
                            vendaPesquisaProdutoTable.getValueAt(selecao[i], 0),
                            vendaPesquisaProdutoTable.getValueAt(selecao[i], 1),
                            vendaPesquisaProdutoTable.getValueAt(selecao[i], 2),//preço
                            vendaPesquisaProdutoTable.getValueAt(selecao[i], 3),
                            quantidade,//quantidade
                            vendaPesquisaProdutoTable.getValueAt(selecao[i], 5)
                    });
                }

                vendaTabelaCarrinho.setModel(model);
                vendaTabelaCarrinho.repaint();

                total = 0;
                for (i = 0; i < vendaTabelaCarrinho.getRowCount(); i++)
                {
                    total = total + Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 2).toString()) *
                            Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 4).toString());
                }

                lblTotalVenda.setText("R$ " + total + "");

                HideAll();
                VendaCarrinhoPanel.setVisible(true);
            }
        });


        // Vontando para a tela do carrinho de compras
        btnVendaCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                VendaCarrinhoPanel.setVisible(true);
            }
        });


        // Botão para ir pra tela de adicionar produtos ao carrinho de compra
        btnVendaAdicionarProduto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                // aqui é onde fica o resultado da busca anterior, ocultamos pois ainda nao foi feita a prox busca
                VendaPesquisaResultado.setVisible(false);
                VendaAdicionarProdutoPanel.setVisible(true);
            }
        });


        btnVendaFinalizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int i;
                double total;
                double valorPago = 0;
                ArrayList items = new ArrayList();

                total = 0;
                for (i = 0; i < vendaTabelaCarrinho.getRowCount(); i++)
                {
                    total = total + Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 2).toString()) *
                            Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 4).toString());

                    items.add(
                            new int[] { Integer.parseInt(vendaTabelaCarrinho.getValueAt(i, 0) +""), // codigo do produto
                                Integer.parseInt(vendaTabelaCarrinho.getValueAt(i, 4)+"") }         // quantidade de tal produto
                    );
                }

                // verificando se valor pago é válido e exibindo msg de erro caso não seja
                try {
                    valorPago = Double.parseDouble(inputValorPago.getText());
                }
                catch (Exception e1)
                {
                    valorPago = 0;
                }

                if (valorPago < total)
                {
                    lblErroValorPago.setVisible(true);
                    return;
                }


                // no final teremos a lista items contendo elementos compostos por codigo e quantidade do produto
                // exemplo...

                ArrayList<Produto> prods = new ArrayList<Produto>();
                for (i = 0; i < items.size(); i++)
                {
                    Produto prod = new Produto() {};

                    int[] a = (int[]) items.get(i);
                    //a[0] -> idProduto     a[1]->quantidade
                    prod.setId(a[0]);
                    prod.setQuantidade(a[1]);
                    prods.add(prod);
                }
                venda.setProdutos(prods);
                //client id = 0 -> cliente nao cadastrado!
                venda.setCliente(cliente);

                ControleVenda cv = new ControleVenda();
                cv.registrarVenda(venda);

                if(cliente.getFidelidade() == 1){
                    //tem fidelidade
                    total = 0;
                    for (i = 0; i < vendaTabelaCarrinho.getRowCount(); i++)
                    {
                        total = total + Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 2).toString()) *
                                Double.parseDouble(vendaTabelaCarrinho.getValueAt(i, 4).toString());
                    }
                    int pontos = (int)(total/10);
                    pontos = pontos * 5;
                    ControleCliente cc = new ControleCliente();
                    cliente.setPontos(cliente.getPontos() + pontos);
                    try {
                        cc.alterar(cliente.getId(),cliente.getNome(),cliente.getCpf(),cliente.getFidelidade(),cliente.getPontos());
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                
                // indo para a tela final, onde informa o troco
                lblTroco.setText("R$ "+(valorPago-total));
                HideAll();
                // painel de sucesso visivel
                panelSucesso.setVisible(true);
                // painel com mensagem de falha oculto
                panelFail.setVisible(false);
                // agora sim vai pra tela final
                panelFinish.setVisible(true);

            }
        });


        // Usuário quer ir para a tela de adicionar produtos ao carrinho de resgate
        btnResgateAdicionarProduto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                // aqui é onde fica o resultado da busca anterior, ocultamos pois ainda nao foi feita a prox busca
                ResgateResultadosView.setVisible(false);
                ResgateAdicionarProdutoPanel.setVisible(true);
            }
        });


        // Finalizando o resgate
        lblResgateFinalizarVenda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int i;
                int total;
                int pontosPagos = 0;
                ArrayList items = new ArrayList();

                total = 0;
                for (i = 0; i < resgateCarrinhoTable.getRowCount(); i++)
                {
                    total = total + Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 2).toString()) *
                            Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 4).toString());

                    items.add(
                            new int[] { Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 0) +""), // codigo do produto
                                    Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 4)+"") }         // quantidade de tal produto
                    );
                }

                // verificando se valor pago é válido e exibindo msg de erro caso não seja
                try {
                    pontosPagos = cliente.getPontos();
                }
                catch (Exception e1)
                {
                    pontosPagos = 0;
                }

                if (pontosPagos < total)
                {
                    lblErroPontosInsuficientes.setVisible(true);
                    return;
                }


                // no final teremos a lista items contendo elementos compostos por codigo e quantidade do produto
                // exemplo...

                ArrayList<Produto> prods = new ArrayList<Produto>();
                for (i = 0; i < items.size(); i++)
                {
                    Produto prod = new Produto() {};

                    int[] a = (int[]) items.get(i);
                    //a[0] -> idProduto     a[1]->quantidade
                    prod.setId(a[0]);
                    prod.setQuantidade(a[1]);
                    prods.add(prod);
                }

                //TODO: DEBITAR OS PONTOS
                //TODO: venda set produtos perfumaria

                venda.setProdutos(prods);
                //cliente id = 0 -> cliente nao cadastrado!
                venda.setCliente(cliente);

                ControleVenda cv = new ControleVenda();
                cv.registrarVenda(venda);

                cliente.setPontos(cliente.getPontos()-total);
                ControleCliente cc = new ControleCliente();
                try {
                    cc.alterar(cliente.getId(),cliente.getNome(),cliente.getCpf(),cliente.getFidelidade(),cliente.getPontos());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                // indo para a tela final, onde informa o troco
                lblTroco.setText(""+(pontosPagos-total)+" pontos");
                HideAll();
                // painel de sucesso visivel
                panelSucesso.setVisible(true);
                // painel com mensagem de falha oculto
                panelFail.setVisible(false);
                // agora sim vai pra tela final
                panelFinish.setVisible(true);

            }
        });



        // Aqui é o evento de pesquisar ITENS DE PERFUMARIA para resgate
        btnResgatePesquisarProduto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean isError = false;
                List<Perfumaria> perfs = new ArrayList<Perfumaria>();
                ControlePerfumaria cp = new ControlePerfumaria();
                try {
                    perfs = cp.buscaProdutoPorNome(inputResgateNomeProduto.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                    isError = true;
                }

                if (!isError) {
                    ResgateResultadosView.setVisible(true);
                    preencheTabelaBuscaResgate(perfs);
                    lblResgateResultadosMensagem.setText("Resultado(s) para o nome \"" + inputResgateNomeProduto.getText() + "\"");
                }

            }
        });


        btnResgateAdicionarAoCarrinho.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int i;
                int total;
                int[] selecao;
                int quantidade = 0;
                int prod_quantidade = 0;

                DefaultTableModel model = (DefaultTableModel) resgateCarrinhoTable.getModel();

                selecao = resgatePesquisaTable.getSelectedRows();


                try {
                    quantidade = Integer.parseInt(inputResgateQuantidade.getText());
                }
                catch (Exception e1)
                {
                    lblResgateErroQuantidade.setVisible(true);
                    return;
                }

                if (quantidade <= 0) {
                    lblResgateErroQuantidade.setVisible(true);
                    return;
                }

                lblResgateErroQuantidade.setVisible(false);

                // Verificando se algum produto selecionado possui quantidade invalida com a solicitada
                for(i = 0; i < selecao.length; i++) {
                    prod_quantidade = Integer.parseInt(resgatePesquisaTable.getValueAt(selecao[i], 4).toString());

                    if (prod_quantidade < quantidade) {
                        lblResgateErroQuantidade.setVisible(true);
                        return;
                    }
                }

                for(i = 0; i < selecao.length; i++)
                {
                    // TODO: se o produto já está adicionado (com o mesmo código) atualiza as quantides somente

                    model.addRow(new Object[]{
                            resgatePesquisaTable.getValueAt(selecao[i], 0),
                            resgatePesquisaTable.getValueAt(selecao[i], 1),
                            resgatePesquisaTable.getValueAt(selecao[i], 2),//preço
                            resgatePesquisaTable.getValueAt(selecao[i], 3),
                            quantidade,//quantidade
                            resgatePesquisaTable.getValueAt(selecao[i], 5)
                    });
                }

                resgateCarrinhoTable.setModel(model);
                resgateCarrinhoTable.repaint();

                total = 0;
                for (i = 0; i < resgateCarrinhoTable.getRowCount(); i++)
                {
                    total = total + Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 2).toString()) *
                            Integer.parseInt(resgateCarrinhoTable.getValueAt(i, 4).toString());
                }

                lblResgateTotalPontos.setText("" + total + " pontos");

                HideAll();
                ResgateCarrinhoPanel.setVisible(true);
            }
        });


        // Voltando para a tela do carrinho de resgate
        btnResgateCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HideAll();
                ResgateCarrinhoPanel.setVisible(true);
            }
        });
    }


    public void iniciaTabelaCarrinho()
    {

        // Iniciando tabela do carrinho de venda
        DefaultTableModel model = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model.addColumn("Id");
        model.addColumn("Nome");
        model.addColumn("Preço");
        model.addColumn("Fornecedor");
        model.addColumn("Quantidade");
        model.addColumn("Validade");

        vendaTabelaCarrinho.setModel(model);
        vendaTabelaCarrinho.repaint();


        // Iniciando tabela do carrinho de resgate
        DefaultTableModel model2 = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model2.addColumn("Id");
        model2.addColumn("Nome");
        model2.addColumn("Pontos");
        model2.addColumn("Fornecedor");
        model2.addColumn("Quantidade");
        model2.addColumn("Validade");

        resgateCarrinhoTable.setModel(model2);
        resgateCarrinhoTable.repaint();

    }


    public void preencheTabelaBuscaVendaRemedio(List<Produto> produto)
    {
        DefaultTableModel model = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model.addColumn("Id");
        model.addColumn("Nome");
        model.addColumn("Preço");
        model.addColumn("Fornecedor");
        model.addColumn("Quantidade disponível");
        model.addColumn("Validade");

        for (Produto p : produto) {

            model.addRow(
                    new Object[]{
                            p.getId() + "",
                            p.getNome(),
                            p.getPreco() + "",
                            p.getFornecedor(),
                            p.getQuantidade() + "",
                            p.getValidade()
                    }
            );
        }

        vendaPesquisaProdutoTable.setModel(model);
        vendaPesquisaProdutoTable.repaint();
    }



    public void preencheTabelaBuscaResgate(List<Perfumaria> produto)
    {
        DefaultTableModel model = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int l, int c) {
                return false;
            }
        };

        model.addColumn("Id");
        model.addColumn("Nome");
        model.addColumn("Pontos");
        model.addColumn("Fornecedor");
        model.addColumn("Quantidade disponível");
        model.addColumn("Validade");

        for (Perfumaria p : produto) {

            model.addRow(
                    new Object[]{
                            p.getId() + "",
                            p.getNome(),
                            p.getPrecoEmPontos() + "",
                            p.getFornecedor(),
                            p.getQuantidade() + "",
                            p.getValidade()
                    }
            );
        }

        resgatePesquisaTable.setModel(model);
        resgatePesquisaTable.repaint();
    }
}
