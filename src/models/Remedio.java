package models;

/**
 * Created by nicolas on 13/10/2016.
 */
public class Remedio extends Produto {
    private String principioAtivo;
    private String dose;
    private String laboratorio;
    private boolean generico;

    public String getPrincipioAtivo(){
        return this.principioAtivo;
    }
    public void setPrincipioAtivo(String principioAtivo){
        this.principioAtivo = principioAtivo;
    }

    public String getDose(){
        return this.dose;
    }
    public void setDose(String dose){
        this.dose = dose;
    }

    public String getLaboratorio(){
        return this.laboratorio;
    }
    public void setLaboratorio(String lab){
        this.laboratorio = lab;
    }

    public boolean getGenerico(){
        return this.generico;
    }
    public void setGenerico(boolean gen){
        this.generico = gen;
    }
}
