package models;


import java.util.ArrayList;

/**
 * Created by nicolas on 13/10/2016.
 */
public class Venda {
    private int id_venda;
    private Cliente cliente;
    private ArrayList<Produto> produtos;

    public int getIdVenda(){
        return this.id_venda;
    }
    public void setIdVenda(int id_venda){
        this.id_venda = id_venda;
    }

    public Cliente getCliente(){
        return this.cliente;
    }
    public void setCliente(Cliente cli){
        this.cliente = cli;
    }

    public ArrayList<Produto> getProdutos(){
        return this.produtos;
    }
    public void setProdutos(ArrayList<Produto> prods){
        this.produtos = prods;
    }
}
