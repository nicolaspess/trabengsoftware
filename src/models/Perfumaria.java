package models;

/**
 * Created by nicolas on 13/10/2016.
 */
public class Perfumaria extends Produto {
    private int precoEmPontos;

    public int getPrecoEmPontos(){
        return this.precoEmPontos;
    }
    public void setPrecoEmPontos(int preco){
        this.precoEmPontos = preco;
    }
}
