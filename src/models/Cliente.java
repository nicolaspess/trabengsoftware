package models;

/**
 * Created by nicolas on 13/10/2016.
 */
public class Cliente {
    private int id;
    private String nome;
    private String cpf;
    private int fidelidade;
    private int pontos;

    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }

    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }

    public String getCpf(){
        return this.cpf;
    }
    public void setCpf(String cpf){
        this.cpf = cpf;
    }

    public int getFidelidade(){
        return this.fidelidade;
    }
    public void setFidelidade(int fid){
        this.fidelidade = fid;
    }

    public int getPontos(){
        return this.pontos;
    }
    public void setPontos(int pontos){
        this.pontos = pontos;
    }

}
