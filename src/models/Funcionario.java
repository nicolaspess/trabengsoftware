package models;

/**
 * Created by nicolas on 13/10/2016.
 */
public class Funcionario {
    private int id;
    private String funcao;
    private String nome;
    private String usuario;
    private String senha;

    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }

    public String getFuncao(){
        return this.funcao;
    }
    public void setFuncao(String f){
        this.funcao = f;
    }

    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }

    public String getUsuario(){
        return this.usuario;
    }
    public void setUsuario(String user){
        this.usuario = user;
    }

    public String getSenha(){
        return this.senha;
    }
    public void setSenha(String senha){
        this.senha = senha;
    }
}
