package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by nicolas on 13/10/2016.
 */
public abstract class Produto {
    private int id;
    private String nome;
    private double preco;
    private String fornecedor;
    private int quantidade;
    private String validade;

    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }

    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }

    public double getPreco(){
        return this.preco;
    }
    public void setPreco(double preco){
        this.preco = preco;
    }

    public String getFornecedor(){
        return this.fornecedor;
    }
    public void setFornecedor(String fornec){
        this.fornecedor = fornec;
    }

    public int getQuantidade(){
        return this.quantidade;
    }
    public void setQuantidade(int quant){
        this.quantidade = quant;
    }

    public String getValidade(){
        return this.validade;
    }
    public void setValidade(String validade){
        this.validade = validade;
    }

}
