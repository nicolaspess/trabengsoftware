package controllers;

import dao.PerfumariaDao;
import models.Produto;
import models.Perfumaria;

import javax.swing.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by nicolas on 28/10/2016.
 */
public class ControlePerfumaria {
    public void salvar(String nome, double preco, String fornecedor, int quantidade, String validade, int precoEmPontos) throws SQLException
    {
        Perfumaria perf = new Perfumaria();
        perf.setNome(nome);
        perf.setPreco(preco);
        perf.setFornecedor(fornecedor);
        perf.setQuantidade(quantidade);
        perf.setValidade(validade);
        perf.setPrecoEmPontos(precoEmPontos);
        new PerfumariaDao().salvar(perf);
    }

    public void alterar(Perfumaria perf)
            throws ParseException, SQLException
    {
        new PerfumariaDao().alterar(perf);
    }

    public void excluir(int id) throws SQLException {
        new PerfumariaDao().excluir(id);
    }

    public List<Perfumaria> buscaProdutoPorNome(String nome) throws SQLException {
        PerfumariaDao dao = new PerfumariaDao();
        return dao.selecionaPerfumaria(nome);
    }


    public List<Perfumaria> listaProdutos() {
        PerfumariaDao dao = new PerfumariaDao();
        try {
            return dao.selecionaPerfumarias();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,
                    "Problemas ao localizar contaton" +
                            e.getLocalizedMessage()
            );
        }
        return null;
    }


    //public ArrayList<Produto> verificarVencimentos(){}

    //public Produto verificarEstoque(Produto prod){}

    //public ArrayList<Produto> gerarRelatorioEstoque(){}
}
