package controllers;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import dao.RemedioDao;
import models.Produto;
import models.Remedio;

import javax.swing.*;

/**
 * Created by nicolas on 13/10/2016.
 */
public class ControleRemedio {

    public void salvar(String nome, double preco, String fornecedor, int quantidade, String validade, String principioAtivo, String dose, String laboratorio, boolean generico) throws SQLException
    {
        Remedio rem = new Remedio();
        rem.setNome(nome);
        rem.setPreco(preco);
        rem.setFornecedor(fornecedor);
        rem.setQuantidade(quantidade);
        rem.setValidade(validade);
        rem.setPrincipioAtivo(principioAtivo);
        rem.setDose(dose);
        rem.setLaboratorio(laboratorio);
        rem.setGenerico(generico);
        new RemedioDao().salvar(rem);
    }

    public void alterar(Remedio remedio)
            throws ParseException, SQLException
    {
        new RemedioDao().alterar(remedio);
    }

    public void excluir(int id) throws SQLException {
        new RemedioDao().excluir(id);
    }

    public List<Remedio> buscaProdutoPorNome(String nome) throws SQLException {
        RemedioDao dao = new RemedioDao();
        return dao.selecionaRemedio(nome);
    }


    public List<Remedio> listaProdutos() {
        RemedioDao dao = new RemedioDao();
        try {
            return dao.selecionaRemedios();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,
                    "Problemas ao localizar contaton" +
                            e.getLocalizedMessage()
            );
        }
        return null;
    }


    //public ArrayList<Produto> verificarVencimentos(){}

    //public Produto verificarEstoque(Produto prod){}

    //public ArrayList<Produto> gerarRelatorioEstoque(){}

}

