package controllers;

import dao.FuncionarioDao;
import models.Funcionario;


import javax.swing.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by nicolas on 13/10/2016.
 */
public class ControleFuncionario {

    public Funcionario fazerLogin(String usuario, String senha) throws SQLException, ParseException{
        FuncionarioDao dao = new FuncionarioDao();
        Funcionario func = new Funcionario();
        func = null;
        func = dao.logar(usuario, senha);
        return func;
    }

    public void salvar(String nome, String funcao, String senha) throws SQLException, ParseException
    {
        Funcionario func = new Funcionario();
        func.setNome(nome);
        func.setFuncao(funcao);
        func.setSenha(senha);

        new FuncionarioDao().salvar(func);
    }

    public void alterar(int id, String nome, String funcao, String senha)
            throws ParseException, SQLException
    {

        Funcionario func = new Funcionario();
        func.setId(id);
        func.setNome(nome);
        func.setFuncao(funcao);
        func.setSenha(senha);

        new FuncionarioDao().alterar(func);
    }

    public void excluir(int id) throws SQLException {
        new FuncionarioDao().excluir(id);
    }

    public List<Funcionario> buscaClientePorNome(String nome) throws SQLException {
        FuncionarioDao dao = new FuncionarioDao();
        return dao.selecionaFuncionario(nome);
    }

    public List<Funcionario> listaClientes() {
        FuncionarioDao dao = new FuncionarioDao();
        try {
            return dao.selecionaFuncionarios();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,
                    "Problemas ao localizar contaton" +
                            e.getLocalizedMessage()
            );
        }
        return null;
    }
}
