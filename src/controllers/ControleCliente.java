package controllers;

import models.Cliente;
import dao.ClienteDao;

import javax.swing.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by nicolas on 13/10/2016.
 */
public class ControleCliente{

    public void salvar(String nome, String cpf, int fidelidade, int pontos) throws SQLException, ParseException
    {
        Cliente cliente = new Cliente();
        cliente.setNome(nome);
        cliente.setCpf(cpf);
        cliente.setFidelidade(fidelidade);
        cliente.setPontos(pontos);

        new ClienteDao().salvar(cliente);
    }

    public void alterar(int id, String nome, String cpf, int fidelidade, int pontos)
            throws ParseException, SQLException
    {

        Cliente cliente = new Cliente();
        cliente.setId(id);
        cliente.setNome(nome);
        cliente.setCpf(cpf);
        cliente.setFidelidade(fidelidade);
        cliente.setPontos(pontos);

        new ClienteDao().alterar(cliente);
    }

    public void excluir(int id) throws SQLException {
        new ClienteDao().excluir(id);
    }

    public Cliente buscaClientePorCpf(String cpf) throws SQLException {
        ClienteDao dao = new ClienteDao();
        return dao.selecionaClienteCpf(cpf);
    }

    public List<Cliente> buscaClientePorNome(String nome) throws SQLException {
        ClienteDao dao = new ClienteDao();
        return dao.selecionaCliente(nome);
    }

    public List<Cliente> listaClientes() {
        ClienteDao dao = new ClienteDao();
        try {
            return dao.selecionaClientes();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,
                    "Problemas ao localizar clientes" +
                            e.getLocalizedMessage()
            );
        }
        return null;
    }
}
