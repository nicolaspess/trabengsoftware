package controllers;

import dao.VendaDao;
import models.Cliente;
import models.Produto;
import models.Venda;
import models.Remedio;
import models.Perfumaria;

import java.sql.SQLException;
import java.util.ArrayList;




/**
 * Created by nicolas on 13/10/2016.
 */
public class ControleVenda {

    //public Venda selecionaVendas(Cliente cli){}

    public void registrarVenda(Venda venda){

        ArrayList<Produto> produtos = venda.getProdutos();
        Produto prod = new Produto(){};

        int idVenda = (int) (System.currentTimeMillis()/1000);

        for(int i=0; i < produtos.size(); i++){
            prod = produtos.get(i);
            try {
                if(venda.getCliente().getId() == 0) {
                    new VendaDao().salvar(prod, idVenda);
                }else{
                    new VendaDao().salvar(prod, idVenda, venda.getCliente().getId());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //public ArrayList<Venda> historicoVendas(Cliente cli){}

}
