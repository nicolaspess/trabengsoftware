-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30-Nov-2016 às 03:14
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `farmacia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `fidelidade` int(1) DEFAULT NULL,
  `pontos` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `fidelidade`, `pontos`) VALUES
(1, 'João Silva', 1, 30),
(2, 'Maria das Dores', 0, 0),
(3, 'Maria das Dores', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE IF NOT EXISTS `funcionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `funcao` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `preco` float NOT NULL,
  `fornecedor` varchar(30) NOT NULL,
  `quantidade` int(10) NOT NULL,
  `validade` date NOT NULL,
  `principio_ativo` varchar(100) DEFAULT NULL,
  `dose` varchar(20) DEFAULT NULL,
  `laboratorio` varchar(50) DEFAULT NULL,
  `generico` tinyint(1) DEFAULT NULL,
  `preco_em_pontos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `tipo`, `nome`, `preco`, `fornecedor`, `quantidade`, `validade`, `principio_ativo`, `dose`, `laboratorio`, `generico`, `preco_em_pontos`) VALUES
(23, 'remedio', 'Benegrip', 90, 'Benefab', 183, '2017-03-12', 'Dipirona + Outras drogas', '4/Dia', 'Benelab', 1, NULL),
(24, 'remedio', 'Benegrip 1', 90, 'Benefab', 183, '2019-03-12', 'Dipirona + Outras drogas', '4/Dia', 'Benelab', 1, NULL),
(25, 'remedio', 'Benegrip 2', 90, 'Benefab', 183, '2020-03-12', 'Dipirona + Outras drogas', '4/Dia', 'Benelab', 1, NULL),
(26, 'remedio', 'Benegrip 3', 90, 'Benefab', 183, '2017-03-12', 'Dipirona + Outras drogas', '4/Dia', 'Benelab', 1, NULL),
(27, 'remedio', 'Benegrip 4', 90, 'Benefab', 183, '2017-03-12', 'Dipirona + Outras drogas', '4/Dia', 'Benelab', 1, NULL),
(28, 'perfumaria', 'Kaiak', 120, 'Natura', 123, '2024-04-14', NULL, NULL, NULL, NULL, 15900),
(29, 'perfumaria', 'Kaiak 1', 120, 'Natura', 123, '2017-04-14', NULL, NULL, NULL, NULL, 15900),
(30, 'perfumaria', 'Kaiak 2', 120, 'Natura', 123, '2060-04-14', NULL, NULL, NULL, NULL, 15900),
(31, 'perfumaria', 'Kaiak 4', 120, 'Natura', 123, '2020-04-14', NULL, NULL, NULL, NULL, 15900),
(32, 'perfumaria', 'Sintonia', 145.99, 'Natura', 1001, '2030-09-20', NULL, NULL, NULL, NULL, 90000),
(33, 'remedio', 'Aspirina', 39, 'Não se sabe', 1000, '2019-03-10', 'Não sei', '10/dia', 'Desconhecido', 1, NULL),
(34, 'perfumaria', 'Agua de cheiro', 120, 'Fornec', 100, '2018-03-10', NULL, NULL, NULL, NULL, 150);

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE IF NOT EXISTS `venda` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_venda` int(10) NOT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `id_produto` int(10) NOT NULL,
  `quantidade` int(10) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
