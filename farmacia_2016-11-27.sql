# ************************************************************
# Sequel Pro SQL dump
# Vers�o 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Base de Dados: farmacia
# Tempo de Gera��o: 2016-11-27 11:59:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `fidelidade` int(1) DEFAULT NULL,
  `pontos` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;

INSERT INTO `cliente` (`id`, `nome`, `fidelidade`, `pontos`)
VALUES
	(1,'João Silva',1,30),
	(2,'Maria das Dores',0,0),
	(3,'Maria das Dores',0,0);

/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela funcionario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `funcionario`;

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `funcao` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela produto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produto`;

CREATE TABLE `produto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `preco` float NOT NULL,
  `fornecedor` varchar(30) NOT NULL,
  `quantidade` int(10) NOT NULL,
  `validade` date NOT NULL,
  `principio_ativo` varchar(100) DEFAULT NULL,
  `dose` varchar(20) DEFAULT NULL,
  `laboratorio` varchar(50) DEFAULT NULL,
  `generico` tinyint(1) DEFAULT NULL,
  `preco_em_pontos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;

INSERT INTO `produto` (`id`, `tipo`, `nome`, `preco`, `fornecedor`, `quantidade`, `validade`, `principio_ativo`, `dose`, `laboratorio`, `generico`, `preco_em_pontos`)
VALUES
	(23,'remedio','Benegrip',90,'Benefab',183,'2017-03-12','Dipirona + Outras drogas','4/Dia','Benelab',1,NULL),
	(24,'remedio','Benegrip 1',90,'Benefab',183,'2019-03-12','Dipirona + Outras drogas','4/Dia','Benelab',1,NULL),
	(25,'remedio','Benegrip 2',90,'Benefab',183,'2020-03-12','Dipirona + Outras drogas','4/Dia','Benelab',1,NULL),
	(26,'remedio','Benegrip 3',90,'Benefab',183,'2017-03-12','Dipirona + Outras drogas','4/Dia','Benelab',1,NULL),
	(27,'remedio','Benegrip 4',90,'Benefab',183,'2017-03-12','Dipirona + Outras drogas','4/Dia','Benelab',1,NULL),
	(28,'perfumaria','Kaiak',120,'Natura',123,'2024-04-14',NULL,NULL,NULL,NULL,15900),
	(29,'perfumaria','Kaiak 1',120,'Natura',123,'2017-04-14',NULL,NULL,NULL,NULL,15900),
	(30,'perfumaria','Kaiak 2',120,'Natura',123,'2060-04-14',NULL,NULL,NULL,NULL,15900),
	(31,'perfumaria','Kaiak 4',120,'Natura',123,'2020-04-14',NULL,NULL,NULL,NULL,15900),
	(32,'perfumaria','Sintonia',145.99,'Natura',1001,'2030-09-20',NULL,NULL,NULL,NULL,90000),
	(33,'remedio','Aspirina',39,'Não se sabe',1000,'2019-03-10','Não sei','10/dia','Desconhecido',1,NULL),
	(34,'perfumaria','Agua de cheiro',120,'Fornec',100,'2018-03-10',NULL,NULL,NULL,NULL,150);

/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
